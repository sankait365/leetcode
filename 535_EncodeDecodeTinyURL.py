import random
class Codec:

    def __init__(self):
        self.code_to_url = {}
        self.alpha_numericals = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890"

    def encode(self, longUrl):
        """Encodes a URL to a shortened URL.
        
        :type longUrl: str
        :rtype: str
        """
        coded_url_key = self.generateKey()
        while coded_url_key in self.code_to_url:
            coded_url_key = self.generateKey()
        self.code_to_url[coded_url_key] = longUrl
        return "http://tinyurl.com/" + coded_url_key
    
    def generateKey(self):
        coded_url_key = ""
        for _ in range(0, 6):
            coded_url_key += random.choice(self.alpha_numericals)
        return coded_url_key

    def decode(self, shortUrl):
        """Decodes a shortened URL to its original URL.
        
        :type shortUrl: str
        :rtype: str
        """
        coded_url_key = shortUrl.split("http://tinyurl.com/")[1]
        return self.code_to_url[coded_url_key]


if __name__ == '__main__':
    url = "https://leetcode.com/problems/design-tinyurl"
    codec = Codec()
    print codec.encode(url)