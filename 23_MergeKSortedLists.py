# Definition for singly-linked list.
from Queue import PriorityQueue

class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """

        head = pointer = ListNode(0)
        queue = PriorityQueue()
        for li in lists:
            if li:
                queue.put((li.val, li))

        while not queue.empty():
            val, node = queue.get() 
            pointer.next = ListNode(val)
            pointer = pointer.next
            node = node.next
            if node:
                queue.put((node.val, node))

        return head.next

if __name__ == "__main__":
    l1 = ListNode(1)
    l1.next = ListNode(4)
    l1.next.next = ListNode(5)

    l2 = ListNode(1)
    l2.next = ListNode(3)
    l2.next.next = ListNode(4)

    l3 = ListNode(2)
    l3.next = ListNode(6)

    temp_l = [l1, l2, l3]

    res = Solution().mergeKLists(temp_l)
    temp = res
    while temp != None:
        print temp.val
        temp = temp.next