class Solution(object):
    def isVowel(self, char):
        vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
        if char in vowels:
            return True
        return False

    def getVowelList(self, string):
        vowels = []
        for char in string:
            if self.isVowel(char):
                vowels.append(char)
        return vowels

    def reverseVowels(self, s):
        """
        :type s: str
        :rtype: str
        """
        vowel_list = self.getVowelList(s)
        list_ptr = len(vowel_list) - 1
        list_s = list(s)
        for i in range(0, len(list_s)):
            if self.isVowel(list_s[i]):
                list_s[i] = vowel_list[list_ptr]
                list_ptr -= 1
        return ''.join(list_s)           

if __name__ == '__main__':
    sol = Solution()
    test = "hello"
    print sol.reverseVowels(test)