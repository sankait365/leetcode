class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """

        res = {}
        for i, num in enumerate(nums):
            diff = target - num
            if diff in res:
                return [res[diff], i]
            else:
                res[num] = i


if __name__ == "__main__":
    nums = [3, 2, 4]
    target = 6
    sol = Solution()
    print sol.twoSum(nums, target)
        