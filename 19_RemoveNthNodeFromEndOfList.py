# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def removeNthFromEnd(self, head, n):
        """
        :type head: ListNode
        :type n: int
        :rtype: ListNode
        """
        node_count = 0
        temp = head
        while temp != None:
            node_count += 1
            temp = temp.next

        if n == 1 and node_count == 1:
            return None
        if n == node_count:
            head = head.next
            return head

        temp = head
        prev = None
        for x in range(0, node_count-n):
            prev = temp
            temp = temp.next

        if temp == head:
            temp = temp.next

        elif temp.next == None:
            prev.next = None

        else:
            prev.next = temp.next

        return head


if __name__ == "__main__":
    sol = Solution()
    t = ListNode(1)
    t.next = ListNode(2)
    #t.next.next = ListNode(3)
    #t.next.next.next = ListNode(4)
    #t.next.next.next.next = ListNode(5)
    x = sol.removeNthFromEnd(t, 2)
    while x != None:
        print x.val
        x = x.next
