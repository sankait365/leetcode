class Solution(object):
    def isPowerOfThree(self, n):
        """
        :type n: int
        :rtype: bool
        """
        if n <= 0:
            return False
        return 1162261467 % n == 0

if __name__ == '__main__':
    sol = Solution()
    print sol.isPowerOfThree(27)