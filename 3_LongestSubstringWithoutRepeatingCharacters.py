class Solution(object):

    def unique(self, s):
        return len(set(s)) == len(s)

    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """

        char_set = set()
        i = 0
        j = 0
        max_len = 0
        len_s = len(s)
        while i < len_s and j < len_s:
            if s[j] not in char_set:
                char_set.add(s[j])
                j += 1
                max_len = max(max_len, j-i)
            else:
                char_set.remove(s[i])
                i += 1
        return max_len

if __name__ == "__main__":
    s = "pwwkew"
    sol = Solution()
    print sol.lengthOfLongestSubstring(s)
