class Solution(object):
    def lengthLongestPath(self, input_str):
        """
        :type input: str
        :rtype: int
        """
        max_len = 0
        count = 0
        level = 1

        is_file = False
        level_mapping = {0: 0}

        i = 0
        while i < len(input_str):
            while input_str[i] == "\t":
                level += 1
                i += 1
            while i < len(input_str) and input_str[i] != "\n":
                if input_str[i] == ".":
                    is_file = True
                count += 1
                i += 1

            if is_file:
                max_len = max(max_len, level_mapping[level - 1] + count)
            else:
                level_mapping[level] = level_mapping[level - 1] + count + 1
            
            count = 0
            level = 1
            is_file = False
            i += 1

        return max_len

if __name__ == '__main__':
    sol = Solution()
    print sol.lengthLongestPath("dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext")
