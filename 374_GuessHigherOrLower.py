# The guess API is already defined for you.
# @param num, your guess
# @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
# def guess(num):

class Solution(object):
    def guessNumber(self, n):
        lo = 1
        hi = n
        while lo <= hi:
            mid = lo + (hi - lo) / 2
            check = guess(mid)
            if check == 0:
                return mid
            elif check < 0:
                hi = mid - 1
            else:
                lo = mid + 1
        return -1
        