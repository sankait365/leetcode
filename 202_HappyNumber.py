import math

class Solution(object):
    def isHappy(self, n):
        """
        :type n: int
        :rtype: bool
        """
        slow = fast = n
        slow = self.sum_digit_sqrs(slow)
        fast = self.sum_digit_sqrs(self.sum_digit_sqrs(fast))
        while slow != fast:
            slow = self.sum_digit_sqrs(slow)
            fast = self.sum_digit_sqrs(self.sum_digit_sqrs(fast))
        if slow == 1:
            return True
        return False

    def sum_digit_sqrs(self, num):
        s = 0
        no = num
        while no > 0:
            digit = no % 10
            s += digit * digit
            no /= 10
        return s

if __name__ == "__main__":
    print Solution().isHappy(19)