import sys
class Solution(object):
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """

        nums = sorted(nums)
        diff = sys.maxint
        res = 0
        for i in range(len(nums) - 2):
            if i > 0 and nums[i] == nums[i-1]:
                continue

            lo = i + 1
            hi = len(nums) - 1
            while lo < hi:
                total = nums[i] + nums[lo] + nums[hi]
                temp_diff = abs(total - target)
                if temp_diff == 0:
                    return total
                if temp_diff < diff:
                    res = total
                    diff = temp_diff

                if total < target:
                    lo += 1
                else:
                    hi -= 1
        return res
        
if __name__ == "__main__":
    sol = Solution()
    print sol.threeSumClosest([-1, 2, 1, -4], 1)
