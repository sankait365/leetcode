import sys

class Solution(object):

    def isNumericChar(self, x):
        if (x >= '0' and x <= '9'):
            return True
        return False

    def myAtoi(self, s):
        """
        :type str: str
        :rtype: int
        """
        res = 0
        st = 0
        s = s.strip()
        if(len(s) == 0):
            return 0

        first_char = s[0]
        sign = 1
        if first_char == '-':
            sign = -1
            st += 1
        if first_char == "+":
            st += 1
        
        max_int = 2147483647
        min_int = -2147483648

        for x in range(st, len(s)):
            if not self.isNumericChar(s[x]):
                break

            res = res*10 + (ord(s[x]) - ord('0'))
        
        if sign*res >= max_int:
            return max_int
        if sign*res <= min_int:
            return min_int
        
        return sign*res

if __name__ == "__main__":
    sol = Solution()
    print sol.myAtoi("2147483648")