class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        if s == p:
            return True
        table = [[False for _ in range(len(p)+1)] for _ in range(len(s)+1)]
        table[0][0] = True
        for col in range(1, len(table[0])):
            if p[col-1] == '*':
                table[0][col] = table[0][col-2]

        for row in range(1, len(table)):
            for col in range(1, len(table[0])):
                if s[row-1] == p[col-1] or p[col-1] == '.':
                    table[row][col] = table[row-1][col-1]
                elif p[col-1] == '*':
                    table[row][col] = table[row][col-2]
                    if s[row-1] == p[col-2] or p[col-2] == '.':
                        table[row][col] = table[row][col] or table[row-1][col]
                else:
                    table[row][col] = False
        return table[len(s)][len(p)]

if __name__ == "__main__":
    print Solution().isMatch("aa", "a*")
