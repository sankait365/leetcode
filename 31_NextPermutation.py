class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        n = len(nums)
        if all(nums[i] >= nums[i+1] for i in xrange(n-1)):
            nums.sort()
        else:
            i = n - 2
            j = n - 1
            while nums[i] >= nums[j] and (i >= 0 and j >= 1):
                i -= 1
                j -= 1
            swapper = n - 1
            print i
            print j
            while swapper >= j:
                if nums[swapper] > nums[i]:
                    temp = nums[i]
                    nums[i] = nums[swapper]
                    nums[swapper] = temp
                    break
                else:
                    swapper -= 1
            nums[j:n] = sorted(nums[j:n])

if __name__ == "__main__":
    sol = Solution()
    arr = [1, 2, 3]
    arr2 = [1, 3, 2]
    arr3 = [1, 5, 1]
    arr4 = [1, 2, 1, 5, 4, 3, 3, 2, 1]
    sol.nextPermutation(arr)
    sol.nextPermutation(arr2)
    sol.nextPermutation(arr3)
    sol.nextPermutation(arr4)
    print arr
    print arr2
    print arr3
    print arr4
