class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        max_int = 2147483647
        min_int = -2147483648
        if x > max_int or x < min_int:
            return 0
        else:
            x_str = str(x)
            x_str = x_str.strip()
            str_index = 0
            rev_str = None
            minus = 1
            if x_str[str_index] == '-':
                rev_str = x_str[:0:-1]
                minus = -1
            else:
                rev_str = x_str[::-1]

            rev_str_int = int(rev_str)
            if rev_str_int > max_int or rev_str_int < min_int:
                return 0
            return minus*rev_str_int

if __name__ == "__main__":
    sol = Solution()
    print sol.reverse(123)