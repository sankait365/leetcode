class Solution(object):
    def generate(self, numRows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """
        if numRows == 0:
            return []
        if numRows == 1:
            return [[1]]
        if numRows == 2:
            return [[1], [1, 1]]
        ret_list = [[1], [1, 1]]
        for i in range(2, numRows):
            new_row = self.create_row(ret_list[i-1])

            ret_list.append(new_row)

        return ret_list
    def create_row(self, lst):
        row = [0] * (len(lst) + 1)
        row[0] = 1
        row[len(row) - 1] = 1

        for j in range(1, len(row) - 1):
            row[j] = lst[j-1] + lst[j] 
        return row

if __name__ == "__main__":
    print Solution().generate(5)

        