# Definition for singly-linked list.
class ListNode(object):     
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        carry = 0
        res = None
        l1_length = 0
        l2_length = 0
        
        l1_temp = l1
        l2_temp = l2

        while l1_temp != None:
            l1_temp = l1_temp.next
            l1_length += 1
        while l2_temp != None:
            l2_temp = l2_temp.next
            l2_length += 1
        
        if l1_length >= l2_length:
            while l1 != None:
                num1 = 0
                if l1 != None:
                    num1 = l1.val
                num2 = 0
                if l2 != None:
                    num2 = l2.val

                ll_entry = None

                s = num1 + num2 + carry        # Add up all the values                                    
                carry_check = s - 10           # Checking the carry
                if carry_check >= 0:            # If this is >= 0, we have a carry
                    carry = 1
                    ll_entry = ListNode(carry_check)
                else:
                    carry = 0
                    ll_entry = ListNode(s)

                if res is None:
                    res = ll_entry
                else:
                    temp = res
                    while temp.next != None:
                        temp = temp.next
                    temp.next = ll_entry
                l1 = l1.next
                if l2 != None:
                    l2 = l2.next

        else:
            while l2 != None:
                num1 = 0
                if l1 != None:
                    num1 = l1.val
                num2 = 0
                if l2 != None:
                    num2 = l2.val

                ll_entry = None

                s = num1 + num2 + carry        # Add up all the values                                    
                carry_check = s - 10           # Checking the carry
                if carry_check >= 0:            # If this is >= 0, we have a carry
                    carry = 1
                    ll_entry = ListNode(carry_check)
                else:
                    carry = 0
                    ll_entry = ListNode(s)

                if res is None:
                    res = ll_entry
                else:
                    temp = res
                    while temp.next != None:
                        temp = temp.next
                    temp.next = ll_entry
                l2 = l2.next
                if l1 != None:
                    l1 = l1.next

        if carry > 0:
            ll_entry = ListNode(carry)
            temp = res
            while temp.next != None:
                temp = temp.next
            temp.next = ll_entry

        return res

if __name__ == "__main__":
    l1 = ListNode(5)
    #l1.next = ListNode(8)
    #l1.next.next = ListNode(3)

    l2 = ListNode(5)
    #l2.next = ListNode(6)
    #l2.next.next = ListNode(4)

    sol = Solution()
    res = sol.addTwoNumbers(l1, l2)
    while res != None:
        print res.val
        res = res.next

