class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        div = 1
        while x/div >= 10:
            div *= 10
        while x != 0:
            front = x/div
            back = x % 10

            if front != back:
                return False
            
            x = (x % div) / 10
            div = div/100
        return True

if __name__ == "__main__":
    sol = Solution()
    print sol.isPalindrome(323)
    print sol.isPalindrome(-323)