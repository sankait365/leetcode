class Solution(object):
    def searchMatrix(self, matrix, target):
        """
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        row_len = len(matrix)
        if row_len == 0:
            return False
        col_len = len(matrix[0])
        if col_len == 0:
            return False

        for row in range(row_len):
            if target >= matrix[row][0] and target <= matrix[row][col_len - 1]:
                if self.binary_search(matrix, row, target, 0, col_len):
                    return True

        return False
        
    def binary_search(self, matrix, row, target, lo, hi):
        while lo <= hi:
            mid = lo + (hi - lo) / 2
            if matrix[row][mid] == target:
                return True
            
            elif matrix[row][mid] < target:
                lo = mid + 1
            
            else:
                hi = mid - 1
        return False

if __name__ == '__main__':

    print Solution().searchMatrix(
        [
            [-1],
            [-1]
        ], 20
    )