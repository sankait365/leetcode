class Node:

    def __init__(self, character):
        self.character = character
        self.children = dict()
        self.end_index = 0

    def add(self, child_node):
        self.children[child_node.character] = child_node


class Trie:

    def __init__(self):
        self.root = Node('0')
        self.words = []
    
    def insert(self, word, index):
        if not word:
            return
        current_node = self.root
        for char in self.normalizeWord(word):
            if char in current_node.children:
                current_node = current_node.children[char]
            else:
                new_node = Node(char)
                current_node.add(new_node)
                current_node = new_node
        current_node.end_index = index
    
    def normalizeWord(self, word):
        return word.strip().lower()

    def dfs(self):
        res = ""
        stack = []
        stack.append(self.root)
        while len(stack) > 0:
            node = stack.pop()
            if node.end_index > 0 or node == self.root:
                if node != self.root:
                    word = self.words[node.end_index - 1]
                    if len(word) > len(res) or (len(word) == len(res) and word < res):
                        res = word
                for n in node.children:
                    stack.append(node.children[n])
        return res

class Solution(object):
    def longestWord(self, words):
        """
        :type words: List[str]
        :rtype: str
        """
        trie = Trie()
        for i in range(len(words)):
            ind = i + 1
            trie.insert(words[i], ind)
        trie.words = words
        return trie.dfs()

if __name__ == '__main__':
    test = ["a", "banana", "app", "appl", "ap", "apply", "apple"]
    print Solution().longestWord(test)
        