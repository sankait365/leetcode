# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def maxDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        return self.maxHelper(root, 0)   
    
    def maxHelper(self, root, l):
        if root == None:
            return l
        return max(self.maxHelper(root.left, l + 1), self.maxHelper(root.right, l + 1)) 

if __name__ == "__main__":
    tree = TreeNode(3)
    tree.left = TreeNode(9)
    tree.left.left = TreeNode(10)
    tree.left.left.left = TreeNode(13)
    tree.right = TreeNode(20)
    tree.right.left = TreeNode(15)
    tree.right.right = TreeNode(7)
    print Solution().maxDepth(tree)
