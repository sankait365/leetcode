class Solution(object):
    def anagramMappings(self, A, B):
        """
        :type A: List[int]
        :type B: List[int]
        :rtype: List[int]
        """
        list_b_mapping = {}
        results = []
        for index, number in enumerate(B, 0):
            if number not in list_b_mapping:
                list_b_mapping[number] = index

        for number in A:
            if number in list_b_mapping:
                results.append(list_b_mapping[number])
        return results
        
if __name__ == "__main__":
    list_A = [12, 28, 46, 32, 50]
    list_B = [50, 12, 32, 46, 28]
    sol = Solution()
    print sol.anagramMappings(list_A, list_B)
