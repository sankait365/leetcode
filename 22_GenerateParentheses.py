class Solution(object):
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        parens = set()
        self.backtrack(parens, "", 0, 0, n)
        return list(parens)

    def backtrack(self, parens, cur, open_i, close_i, max_p):
        if len(cur) == max_p * 2:
            parens.add(cur)
            return
        if open_i < max_p:
            self.backtrack(parens, cur + "(", open_i+1, close_i, max_p)
        if close_i < open_i:
            self.backtrack(parens, cur + ")", open_i, close_i + 1, max_p)
            


if __name__ == '__main__':
    print Solution().generateParenthesis(4)

            
