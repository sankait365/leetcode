class Node:
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.prev = None
        self.next = None

class LRUCache:
    def __init__(self, capacity):
        self.capacity = capacity
        self.lr_dict = dict()
        self.head = Node(0, 0)
        self.tail = Node(0, 0)
        self.head.next = self.tail
        self.tail.prev = self.head
    
    def get(self, key):
        if key in self.lr_dict:
            # Refresh
            node = self.lr_dict[key]
            self.remove_node(node)
            self.add(node)
            return node.val
        return -1

    def put(self, key, val):
        if key in self.lr_dict:
            self.remove_node(self.lr_dict[key])
        node = Node(key, val)
        self.add(node)
        self.lr_dict[key] = node
        if len(self.lr_dict) > self.capacity:
            mark = self.head.next
            self.remove_node(mark)
            del self.lr_dict[mark.key]

    def remove_node(self, node):
        p = node.prev
        n = node.next
        p.next = n
        n.prev = p
    
    def add(self, node):
        p = self.tail.prev
        node.prev = p
        node.next = self.tail
        p.next = node
        self.tail.prev = node
    