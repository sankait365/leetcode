class StackNode(object):
    def __init__(self, val):
        self.value = val
        self.previous_min = None

class MinStack(object):

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.current_min = None
        self.stack = []
        

    def push(self, x):
        """
        :type x: int
        :rtype: void
        """
        new_node = StackNode(x)
        if len(self.stack) == 0:
            self.current_min = x
            self.stack.append(new_node)
        else:
            if x < self.current_min:
                new_node.previous_min = self.current_min
                self.current_min = x
            self.stack.append(new_node)

    def pop(self):
        """
        :rtype: void
        """
        if len(self.stack) > 0:
            popped_node = self.stack.pop()
            if popped_node.previous_min:
                self.current_min = popped_node.previous_min
            return popped_node.value
        else:
            print "Stack empty"
        

    def top(self):
        """
        :rtype: int
        """
        if len(self.stack) > 0:
            return self.stack[len(self.stack) - 1].value
        else:
            print "Stack empty"
        

    def getMin(self):
        """
        :rtype: int
        """
        return self.current_min

if __name__ == '__main__':
    min_stack = MinStack()
    min_stack.push(0)
    min_stack.push(1)
    min_stack.push(0)
    print min_stack.getMin()
    print min_stack.pop()
    print min_stack.getMin()