class Solution(object):
    def __init__(self):
        self.ways = {1: 1, 
                     2: 2}

    def climbStairs(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n < 0:
            return 0
        if n == 0:
            return 1
        
        if n not in self.ways:
            self.ways[n] = self.climbStairs(n - 1) + self.climbStairs(n - 2)
        return self.ways[n]

if __name__ == "__main__":
    print Solution().climbStairs(100)