class Solution(object):
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        romans = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C':100, 'D':500, 'M':1000}
        val = 0
        for i in range(len(s)):
            if i > 0 and romans[s[i]] > romans[s[i-1]]:
                val += romans[s[i]] - 2 * romans[s[i - 1]]
            else:
                val += romans[s[i]]
        return val

if __name__ == "__main__":
    sol = Solution()
    print sol.romanToInt("VII")