class Solution(object):
    def findDisappearedNumbers(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        n = len(nums)

        result_set = set()
        for i in range(1, n + 1):
            result_set.add(i)
        
        for num in nums:
            if num in result_set:
                result_set.remove(num)
        return list(result_set)

if __name__ == '__main__':
    sol = Solution()
    sample = [4,3,2,7,8,2,3,1]
    print sol.findDisappearedNumbers(sample)