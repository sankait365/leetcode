class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        res = 0
        for num in nums:
            res ^= num
        
        return res

if __name__ == '__main__':
    test_nums = [4,1,2,1,2]
    sol = Solution()
    print sol.singleNumber(test_nums)
        