class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        if n <= 0:
            return "-1"

        prev = '1'
        for _ in range(n-1):
            prev = self.compute_next(prev)
        return prev

    def compute_next(self, p):
        i = 0
        number = p[0]
        count = 0
        res_str = ""
        while i < len(p):
            if p[i] == number:
                count += 1
                i += 1
            else:
                res_str += str(count) + str(number)
                number = p[i]
                count = 0

        res_str += str(count) + str(number)
        return res_str

if __name__ == "__main__":
    print Solution().countAndSay(10)