class Solution(object):
    def searchRange(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        left_i = self.extreme_binary_search(nums, target, True)
        if left_i == len(nums) or nums[left_i] != target:
            return [-1, -1]
        return [left_i, self.extreme_binary_search(nums, target, False) - 1]

    def extreme_binary_search(self, nums, target, left):
        lo = 0
        hi = len(nums)
        while lo < hi:
            mid = (lo + hi) / 2
            if nums[mid] > target or (left and nums[mid] == target):
                hi = mid
            else:
                lo = mid + 1
        return lo

if __name__ == "__main__":
    nums = [5,7,7,8,8,10]
    target = 8
    print Solution().searchRange(nums, target)