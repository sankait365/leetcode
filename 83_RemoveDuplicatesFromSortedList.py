# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        current = head
        while current:
            forward = current.next
            if forward and forward.val == current.val:
                current.next = forward.next
                forward = forward.next
            else:
                current = forward
        return head

    def printAll(self, head):
        temp = head
        while temp:
            print temp.val
            temp = temp.next

if __name__ == "__main__":
    li = ListNode(1)
    li.next = ListNode(1)
    li.next.next = ListNode(2)
    li.next.next.next = ListNode(3)
    li.next.next.next.next = ListNode(3)
    s = Solution()
    li = s.deleteDuplicates(li)
    s.printAll(li)
