# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def isSameTree(self, p, q):
        """
        :type p: TreeNode
        :type q: TreeNode
        :rtype: bool
        """

        return self.traverse(p, q)

    def traverse(self, tree1, tree2):
        if tree1 == None and tree2 == None:
            return True
        if tree1 == None or tree2 == None:
            return False
        if tree1.val == tree2.val:
            return self.traverse(tree1.left, tree2.left) and self.traverse(tree1.right, tree2.right)
        return False

if __name__ == "__main__":
    t1 = TreeNode(1)
    t1.left = TreeNode(1)

    t2 = TreeNode(1)
    t2.right = TreeNode(1)

    print Solution().isSameTree(t1, t2)

