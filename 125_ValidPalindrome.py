class Solution(object):
    def isPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        s = ''.join(i for i in s if i.isalnum()).lower()
        return s == s[::-1]

if __name__ == "__main__":
    inp = "A man, a plan, a canal: Panama"
    print Solution().isPalindrome(inp)