class Solution(object):
    def search(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        n = len(nums)
        if n == 0:
            return -1

        pivot = self.find_pivot(nums, 0, n-1)

        if pivot == -1:
            return self.binary_search(nums, target, 0, n-1)

        if nums[pivot] == target:
            return pivot
        if nums[0] <= target:
            # Check left of pivot
            return self.binary_search(nums, target, 0, pivot-1)
        else:
            return self.binary_search(nums, target, pivot+1, n-1)
    
    def binary_search(self, nums, target, start, end):
        if end < start:
            return -1
        mid = int((start+end) / 2)
        if nums[mid] == target:
            return mid
        elif target > nums[mid]:
            return self.binary_search(nums, target, mid + 1, end)
        else:
            return self.binary_search(nums, target, start, mid - 1)

    def find_pivot(self, nums, start, end):

        if end < start:
            return -1
        if end == start:
            return end

        mid = int((start+end) / 2)
        if mid < end and nums[mid] > nums[mid+1]:
            return mid
        if mid > start and nums[mid] < nums[mid-1]:
            return mid-1
        if nums[start] >= nums[mid]:
            return self.find_pivot(nums, start, mid-1)
        return self.find_pivot(nums, mid+1, end)

if __name__ == "__main__":
    sol = Solution()
    arr = [4, 5, 6, 7, 0, 1, 2]
    arr2 = [1, 3]
    print sol.search(arr, 10)
    print sol.search(arr2, 1)
