class Solution(object):
    def judgeCircle(self, moves):
        """
        :type moves: str
        :rtype: bool
        """
        current_position = [0,0]

        for move in moves:
            if move == "U":
                current_position[1] += 1
            elif move == "D":
                current_position[1] -= 1
            elif move == "L":
                current_position[0] -= 1
            elif move == "R":
                current_position[0] += 1
            else:
                pass

        if current_position[0] == 0 and current_position[1] == 0:
            return True
        return False

if __name__ == '__main__':
    input_robot = "LL"
    sol = Solution()
    print sol.judgeCircle(input_robot)