class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        max_so_far = max_sub = nums[0]
        for i in range(1, len(nums)):
            max_sub = max(nums[i], max_sub + nums[i])
            max_so_far = max(max_so_far, max_sub)
        return max_so_far

if __name__ == "__main__":
    print Solution().maxSubArray([-2,1,-3,4,-1,2,1,-5,4])