from collections import OrderedDict
class Solution(object):
    def firstUniqChar(self, s):
        """
        :type s: str
        :rtype: int
        """
        char_counter = OrderedDict()
        for ltr in s:
            if ltr not in char_counter:
                char_counter[ltr] = 0
            char_counter[ltr] += 1

        first_unique_char = None
        for key in char_counter:
            if char_counter[key] == 1:
                first_unique_char = key
                break
        if first_unique_char is None:
            return -1
        return s.index(first_unique_char)

if __name__ == '__main__':
    str_s = "loveleetcode"
    sol = Solution()
    print sol.firstUniqChar(str_s)