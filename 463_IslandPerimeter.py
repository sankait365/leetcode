class Solution(object):
    def islandPerimeter(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        final_perimeter = 0
        n_rows = len(grid)
        n_cols = len(grid[0])
        for i in range(n_rows):
            for j in range(n_cols):
                if grid[i][j] == 1:
                    final_perimeter += self.checkLocalPerimeter(grid, i, j, n_rows, n_cols)
        return final_perimeter

    def checkLocalPerimeter(self, grid, i, j, n_rows, n_cols):
        #Top
        local_perimeter = 4
        t_i = i - 1
        t_j = j
        if t_i in range(n_rows) and t_j in range(n_cols):
            if grid[t_i][t_j] == 1:
                local_perimeter -= 1
        #Bottom
        b_i = i + 1
        b_j = j
        if b_i in range(n_rows) and b_j in range(n_cols):
            if grid[b_i][b_j] == 1:
                local_perimeter -= 1
        #Left
        l_i = i
        l_j = j - 1
        if l_i in range(n_rows) and l_j in range(n_cols):
            if grid[l_i][l_j] == 1:
                local_perimeter -= 1
        #Right
        r_i = i
        r_j = j + 1
        if r_i in range(n_rows) and r_j in range(n_cols):
            if grid[r_i][r_j] == 1:
                local_perimeter -= 1

        return local_perimeter


if __name__ == '__main__':
    matrix = [[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]
    sol = Solution()
    print sol.islandPerimeter(matrix)