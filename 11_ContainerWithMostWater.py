class Solution(object):
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        """
        i = 0
        j = len(height) - 1
        max_area = 0
        while i <= j:
            left = height[i]
            right = height[j]

            area = min(left, right) * (j-i)
            if area > max_area:
                max_area = area
            
            if left < right:
                i += 1
            else:
                j -= 1
            
        return max_area


if __name__ == "__main__":
    sol = Solution()
    print sol.maxArea([1, 2, 4, 3, 2])
