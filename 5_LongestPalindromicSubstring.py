class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        # Adapted from https://leetcode.com/problems/longest-palindromic-substring/discuss/2954/Python-easy-to-understand-solution-with-comments-(from-middle-to-two-ends).
        res = ""
        for i in range(len(s)):
            res = max(self.helper(s, i, i), self.helper(s, i, i + 1), res, key=len)
        return res

    def helper(self, s, l, r):
        while 0 <= l and r < len(s) and s[l] == s[r]:
            l -= 1
            r += 1
        return s[l+1:r]

if __name__ == "__main__":
    sol = Solution()
    print sol.longestPalindrome("321012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210123210012321001232100123210123")


# Initial attempt that timed out
"""
n = len(s)
start = 0
end = 0
entry_store = []
for x in range(0, n):
    entry_store.append([])
    for y in range(0, n):
        entry_store[x].append(False)

# All single chars are palindromes
max_len = 1
i = 0
while i < n:
    entry_store[i][i] = True
    i += 1

i = 0
while i < n - 1:
    if s[i] == s[i+1]:
        max_len = 2
        start = i
        end = i + 1
        entry_store[i][i+1] = True
    i += 1

k = 3
while k <= n:
    i = 0
    while i < (n - k + 1):
        j = i + k - 1
        if entry_store[i+1][j-1] and s[i] == s[j]:
            entry_store[i][j] = True
            if k > max_len:
                max_len = k
                start = i
                end = j
        i += 1
    k += 1

return s[start:end+1]
"""