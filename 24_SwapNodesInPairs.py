# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def swapPairs(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head:
            return None
        p1 = head
        p2 = head.next

        while p1 and p2:
            temp = p1.val
            p1.val = p2.val
            p2.val = temp
            if p1.next:
                p1 = p1.next.next
            if p2.next:
                p2 = p2.next.next
        
        return head

if __name__ == "__main__":
    l1 = ListNode(1)
    l1.next = ListNode(2)
    l1.next.next = ListNode(3)
    l1.next.next.next = ListNode(4)
    l1.next.next.next.next = ListNode(5)

    res = Solution().swapPairs(l1)
    temp = res
    while temp != None:
        print temp.val
        temp = temp.next