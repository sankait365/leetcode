# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution(object):
    def mergeTwoLists(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """

        temp_l1 = l1
        temp_l2 = l2
        new_list = None

        if l1 == None:
            return l2
        if l2 == None:
            return l1

        temp_l1 = l1
        temp_l2 = l2

        while temp_l1 != None and temp_l2 != None:
            if temp_l1.val <= temp_l2.val:
                    new_list = self.insert(new_list, temp_l1.val)
                    temp_l1 = temp_l1.next
            else:
                new_list = self.insert(new_list, temp_l2.val)
                temp_l2 = temp_l2.next

        if temp_l1 != None:
            temp_node = new_list
            while temp_node.next != None:
                temp_node = temp_node.next
            temp_node.next = temp_l1

        if temp_l2 != None:
            temp_node = new_list
            while temp_node.next != None:
                temp_node = temp_node.next
            temp_node.next = temp_l2
            
        return new_list

    def insert(self, node, val):
        temp = ListNode(val)
        if node == None:
            node = temp
            return node
        temp_node = node
        while temp_node.next != None:
            temp_node = temp_node.next
        temp_node.next = temp
        return node
        
if __name__ == "__main__":
    list1 = ListNode(1)
    list1.next = ListNode(2)
    list1.next.next = ListNode(4)

    list2 = ListNode(1)
    list2.next = ListNode(3)
    list2.next.next = ListNode(4)

    sol = Solution()
    new_list = sol.mergeTwoLists(list1, list2)

    while new_list != None:
        print new_list.val
        new_list = new_list.next
