class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        if len(needle) == 0:
            return 0

        i = 0
        while i < len(haystack):
            if haystack[i] == needle[0] and (i + len(needle)) <= len(haystack):
                section = haystack[i: i + len(needle)]
                if section == needle:
                    return i
            i += 1
        return -1

if __name__ == '__main__':
    sol = Solution()
    print sol.strStr("helol", "ll")