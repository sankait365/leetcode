class Solution(object):
    def findTheDifference(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        ltr_dict = dict()
        for ltr in s:
            if ltr not in ltr_dict:
                ltr_dict[ltr] = 0
            ltr_dict[ltr] += 1

        for ltr in t:
            if ltr not in ltr_dict:
                return ltr
            else:
                ltr_dict[ltr] -= 1
        for key in ltr_dict:
            if ltr_dict[key] < 0:
                return key

if __name__ == '__main__':
    str_s = "abcd"
    str_t = "abcde"
    sol = Solution()
    print sol.findTheDifference(str_s, str_t)