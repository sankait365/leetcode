import sys
class Solution(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        if len(nums1) > len(nums2):
            return self.partition(nums2, nums1)
        return self.partition(nums1, nums2)
    
    def partition(self, smaller_list, larger_list):
        x = len(smaller_list)
        y = len(larger_list)
        lo = 0
        hi = x
        while lo <= hi:
            partitionX = (lo + hi) / 2
            partitionY = (x + y + 1)/2 - partitionX
            
            max_left_x = self.get_max_left(smaller_list, partitionX)
            min_right_x = self.get_min_right(smaller_list, partitionX, x)

            max_left_y = self.get_max_left(larger_list, partitionY)
            min_right_y = self.get_min_right(larger_list, partitionY, y)

            if max_left_x <= min_right_y and max_left_y <= min_right_x:
                if (x + y) % 2 == 0:
                    return (max(max_left_x, max_left_y) + min(min_right_x, min_right_y)) / 2.0
                else:
                    return max(max_left_x, max_left_y)
            elif max_left_x > min_right_y:
                hi = partitionX - 1
            else:
                lo = partitionX + 1

    def get_max_left(self, lst, partition):
        if partition == 0:
            return -1*sys.maxint
        return lst[partition-1]

    def get_min_right(self, lst, partition, length):
        if partition == length:
            return sys.maxint
        return lst[partition]

if __name__ == '__main__':
    sol = Solution()
    a1 = [1, 9]
    a2 = [3]
    print sol.findMedianSortedArrays(a1, a2)