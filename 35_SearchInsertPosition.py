class Solution(object):
    def searchInsert(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        if target <= nums[0]:
            return 0
        
        i = 0
        j = 1
        while j < len(nums):
            if nums[j] == target or (nums[i] < target and target < nums[j]):
                return j
            i += 1
            j += 1

        return j
            
if __name__ == "__main__":
    print Solution().searchInsert([0,3,5,6], 0)