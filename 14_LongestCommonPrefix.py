class Solution(object):
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        if strs == None or len(strs) == 0:
            return ""
        for i in range(0, len(strs[0])):
            for j in range(1, len(strs)):
                if i == len(strs[j]) or strs[j][i] != strs[0][i]:
                    return strs[0][0:i]

        return strs[0]

if __name__ == "__main__":
    sol = Solution()
    a1 = "hello"
    a2 = "hel"
    a3 = "hell"
    print sol.longestCommonPrefix([a1, a2, a3])