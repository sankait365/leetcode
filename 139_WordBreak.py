class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        word_set = set(wordDict)
        i = 0
        sub_str = ""
        len_s = len(s)
        while i < len_s:
            sub_str += s[i]
            if sub_str in word_set:
                s = s[i+1:]
                print s
                sub_str = ""
                i = 0
                len_s = len(s)
            else:
                i += 1

        return len(s) == 0
            

if __name__ == '__main__':
    sol = Solution()
    test_s = "aaaaaaa"
    test_l = ["aaaa","aaa"]
    print sol.wordBreak(test_s, test_l)