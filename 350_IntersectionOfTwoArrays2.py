class Solution(object):
    def intersect(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        counter = dict()
        for num in nums1:
            if num not in counter:
                counter[num] = 0
            counter[num] += 1
        
        result = []
        for num in nums2:
            if num in counter and counter[num] > 0:
                result.append(num)
                counter[num] -= 1

        return result

if __name__ == '__main__':
    sol = Solution()
    print sol.intersect([1, 2, 2, 1], [2, 2])
