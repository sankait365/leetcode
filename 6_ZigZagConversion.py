class Solution(object):
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """

        if numRows == 1:
            return s

        matrix = []
        for x in range(0, numRows):
            matrix.append([])
        
        direction = 'd'
        row_to_insert = 0
        x = 0
        while x < len(s):
            if row_to_insert >= 0 and row_to_insert < numRows:
                matrix[row_to_insert].append(s[x])
                if direction == 'd':
                    row_to_insert += 1
                elif direction == 'u':
                    row_to_insert -= 1
            else:
                if row_to_insert < 0 and direction == 'u':
                    row_to_insert += 2
                    direction = 'd'
                elif row_to_insert >= numRows and direction == 'd':
                    row_to_insert -= 2
                    direction = 'u'
                x -= 1
            x += 1

        res = ""
        for i in range(0, numRows):
            res += ''.join(matrix[i])

        return res


if __name__ == "__main__":
    sol = Solution()
    print sol.convert("AB", 1)
