from itertools import product
class Solution(object):

    mapping = {
        '1' : [],
        '2' : ['a', 'b', 'c'],
        '3' : ['d', 'e', 'f'],
        '4' : ['g', 'h', 'i'],
        '5' : ['j', 'k', 'l'],
        '6' : ['m', 'n', 'o'],
        '7' : ['p', 'q', 'r', 's'],
        '8' : ['t', 'u', 'v'],
        '9' : ['w', 'x', 'y', 'z'],
        '0' : []
    }

    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        res = []
        n = len(digits)
        if n == 0:
            return []
        if n == 1:
            return self.mapping[digits[0]]

        for x in range(0, n):
            if digits[x] == '0' or digits[x] == '1':
                continue
            if x == 0:
                res += self.mapping[digits[x]]
            else:
                temp_l = list(product(res, self.mapping[digits[x]]))
                temp_res = []
                for item in temp_l:
                    temp_res.append(''.join(item))
                res = temp_res
        return res


if __name__ == "__main__":
    sol = Solution()
    print sol.letterCombinations("23")
