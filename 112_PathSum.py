# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def hasPathSum(self, root, s):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: bool
        """
        if not root:
            return False
        if not root.left and not root.right and s-root.val == 0:
            return True
        
        return self.hasPathSum(root.left, s - root.val) or self.hasPathSum(root.right, s - root.val)

if __name__ == "__main__":
    node = TreeNode(5)
    node.left = TreeNode(4)
    node.right = TreeNode(8)
    node.left.left = TreeNode(11)
    node.left.left.left = TreeNode(7)
    node.left.left.right = TreeNode(2)
    node.right.left = TreeNode(13)
    node.right.right = TreeNode(4)
    node.right.right.right = TreeNode(1)
    print Solution().hasPathSum(node, 22)