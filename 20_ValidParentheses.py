class Solution(object):
    def isValid(self, s):
        stack = []
        match = {'(': ')', '[': ']', '{': '}'}
        for paren in s:
            if paren in ['(', '[', '{']:
                stack.append(paren)
            elif not stack or match[stack.pop()] != paren:
                return False
        return not stack

if __name__ == "__main__":
    sol = Solution()
    print sol.isValid("]")
    print sol.isValid("(){()}[[]]")
    print sol.isValid("()()(]")
