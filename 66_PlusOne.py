class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        len_digits = len(digits)
        if len_digits == 0:
            return [1]

        carry = 1
        iterator = len_digits - 1
        while carry != 0 and iterator >= 0:
            if digits[iterator] < 9:
                digits[iterator] += carry
                carry = digits[iterator]/10
            else:
                digits[iterator] = 0
                carry = 1
            iterator -= 1

        if carry == 0:
            return digits
        else:
            digits.insert(0, carry)
            return digits

        

if __name__ == '__main__':
    arr_in = [1, 2, 3]
    sol = Solution()
    print sol.plusOne(arr_in)
    print sol.plusOne([1, 2, 9])
    print sol.plusOne([9, 9, 9])
