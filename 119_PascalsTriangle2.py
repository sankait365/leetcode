class Solution(object):
    def getRow(self, rowIndex):
        """
        :type rowIndex: int
        :rtype: List[int]
        """
        if rowIndex == 0:
            return [1]
        if rowIndex == 1:
            return [1, 1]
        prev_row = [1, 1]
        for i in range(1, rowIndex):
            prev_row = self.create_row(prev_row)
        return prev_row

    def create_row(self, lst):
        row = [0] * (len(lst) + 1)
        row[0] = 1
        row[len(row) - 1] = 1

        for j in range(1, len(row) - 1):
            row[j] = lst[j-1] + lst[j] 
        return row

if __name__ == "__main__":
    print Solution().getRow(3)