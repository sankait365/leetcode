import math
class Solution(object):
    def mySqrt(self, x):
        """
        :type x: int
        :rtype: int
        """
        return int(math.sqrt(x))

if __name__ == "__main__":
    print Solution().mySqrt(3)
