class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        i = 0
        for j in range(len(nums)):
            if nums[j] != val:
                nums[i] = nums[j]
                i += 1
        return i
 
if __name__ == '__main__':
    sol = Solution()
    test_nums = [3,2,3,2]
    test_val = 3
    print sol.removeElement(test_nums, test_val)